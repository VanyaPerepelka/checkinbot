package org.dubler.constants;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
@Slf4j
public class Commands {

    public static final String START = "/start";

    public static final String NEW_USER = "реестрація\uD83E\uDD79";

    //public static final String NEW_SHEET = "додати графік\uD83D\uDCBC{в розробці}";

    public static final String START_SHIFT = "чек ін\uD83D\uDE49";

    public static final String CLOSE_SHIFT = "чек аут\uD83D\uDE48";

    public static final String MY_DATA = "моя інфаℹ️";

    //public static final String EDIT_USER = "редагувати юзера\uD83D\uDCDD{в розробці}";

    //public static final String EDIT_MY_DATA = "редагувати мою інфу\uD83D\uDCDD{в розробці}";

    public static final String NOTIFY_ALL = "алерт!\uD83D\uDEA8";

    public static final String STATISTICS = "переглянути мою статистику\uD83D\uDD75️\u200D";

    //public static final String ADMIN_SHIFT_INFO = "стягнути чекіни команди\uD83D\uDD78️";

    public static final String ABOUT_BOT = "мануал по боту";

    public final List<String> commandsList;

    public final List<String> fields;

    public Commands(){
        this.commandsList = getAllCommands();
        this.fields = getFields();
    }

    private List<String> getAllCommands(){
        var res = getFields();
        res.remove(START);
        res.remove(NEW_USER);
        res.remove(log.toString());
        return res;
    }

    private List<String> getFields(){
        ArrayList<String> comms = new ArrayList<>();
        Arrays.stream(this.getClass().getDeclaredFields()).forEach(field -> {
            try {
                if (field.get(this) != null) comms.add(String.valueOf(field.get(this)));
            } catch (IllegalAccessException e) {
                log.error(e.getMessage());
            }
        });
        return comms;
    }

    public boolean notApplicableCommand(String command){
        return !fields.contains(command);
    }

}
