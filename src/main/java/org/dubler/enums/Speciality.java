package org.dubler.enums;

public enum Speciality {
    BAR,
    KITCHEN,
    SERVICE,
    MANAGEMENT,
    CLEANING,
    BOT_OWNER
}
