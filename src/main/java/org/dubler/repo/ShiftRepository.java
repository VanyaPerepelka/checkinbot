package org.dubler.repo;

import org.dubler.models.entities.Shift;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface ShiftRepository extends JpaRepository<Shift, Long> {

    @Query(value = "select * from shifts_table where user_id=?1", nativeQuery = true)
    List<Shift> getShiftsByUserId(@Param("user_id") Long userId);

    @Query(value = "select * from shifts_table where CAST(start_time as date )=?1", nativeQuery = true)
    List<Shift> getShiftsByDate(@Param("start_time") LocalDate startTime);

    @Query(value = "select * from shifts_table where CAST(start_time as date)=?1 and user_id=?2", nativeQuery = true)
    Shift getUserShiftByDate(@Param("start_time") LocalDateTime startTime, @Param("user_id") Long userId);
}
