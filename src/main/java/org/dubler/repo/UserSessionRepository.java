package org.dubler.repo;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.dubler.models.session.UserSession;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;

@Repository
public interface UserSessionRepository extends JpaRepository<UserSession, Long> {

    @Transactional
    @JsonDeserialize
    @Query(value = "select * from session_table where chat_id=?1" ,nativeQuery = true)
    UserSession findByChatId(@NotNull @Param("chat_id") Long chatId);

    @Transactional
    @JsonDeserialize
    @Query(value = "select * from session_table where id=?1", nativeQuery = true)
    UserSession findSessionById(@NotNull @Param("id") Long sessionId);
}
