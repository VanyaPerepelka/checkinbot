package org.dubler.repo;

import org.dubler.models.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UsersRepository extends JpaRepository<User, Long> {

    @Query( value = "select * from users_table where chat_id=?1", nativeQuery = true)
    User getUserByChatId(@Param("chatId") Long chatId);

    @Query(value = "select * from users_table where firstname=?1 and lastname=?2", nativeQuery = true)
    User getUserByPersonalData(@Param("firstname") String firstName, @Param("lastname") String lastname);

    @Query(value  = "select * from users_table where speciality=?1", nativeQuery = true)
    List<User> getUserBySpeciality(@Param("speciality") String speciality);
}
