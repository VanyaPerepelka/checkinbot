package org.dubler.handlers.registration;

import lombok.extern.slf4j.Slf4j;
import org.dubler.constants.Commands;
import org.dubler.enums.Speciality;
import org.dubler.handlers.AbstractUserRequestHandler;
import org.dubler.services.telegram.utils.KeyboardBuilder;
import org.dubler.models.entities.User;
import org.dubler.models.request.UserRequest;
import org.dubler.models.session.UserSession;
import org.dubler.services.implementation.UserService;
import org.dubler.services.telegram.TelegramService;
import org.dubler.services.implementation.UserSessionService;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

@Component
@Slf4j
public class RegistrationHandler extends AbstractUserRequestHandler {

    private static final String APPLY_BUTTON = "Прийняти";

    private static final String DENY_BUTTON = "Спочатку";

    public RegistrationHandler(TelegramService telegramService,
                               UserService userService,
                               UserSessionService sessionService,
                               Commands commands,
                               KeyboardBuilder builder){
        this.telegramService = telegramService;
        this.userService = userService;
        this.sessionService = sessionService;
        this.builder = builder;
        this.commands = commands;
    }
    @Override
    public boolean isApplicable(UserRequest request) {
        return isCommand(request.getUpdate(), Commands.NEW_USER)
                || List.of(RegistrationStatus.values()).contains(request.getUserSession().getConversationState())
        ;
    }

    @Override
    public void handle(UserRequest dispatchedRequest) {

        Long chatId = dispatchedRequest.getChatId();
        String inputText = dispatchedRequest.getUpdate().getMessage().getText();
        String feedbackText = "";
        UserSession session = sessionService.getSession(chatId);

        User user = session.getUser();

        ReplyKeyboardMarkup keyboardMarkup = null;
        Enum<?> registrationStatus = session.getConversationState();

        if (userService.getUserByChatId(chatId).isRegistered()){
            feedbackText = "знайшов тебе в базі. Якщо хочеш редагувати свою інфу - сконтактуй менеджера";
            keyboardMarkup = builder.buildVariableMenu(commands.commandsList, true);
        } else if (registrationStatus.equals(RegistrationStatus.NEW) || !(registrationStatus instanceof RegistrationStatus)) {

            user.setUsername(dispatchedRequest.getUpdate().getMessage().getFrom().getUserName());
            user.setTotalBalance(0D);
            session.setUser(user);
            user.setShifts(new HashSet<>());
            user.setTotalBalance(0D);
            user.setWorkHours(0L);

            session.setConversationState(RegistrationStatus.NAME);
            feedbackText = """
                    Мені треба твоє імʼя та прізвище.
                    Якщо тебе звати Джон Джонсон, я очікую що ти напишеш це сами в такий спосіб.
                    Типу "Джон Джонсон"
                    """;

        } else if (registrationStatus.equals(RegistrationStatus.NAME)) {
            String[] nameData = inputText.split(" ");
            if (nameData.length != 2) {
                feedbackText = "щось дивне я отримав, " +
                        "я хочу отримати у відповідь два слов: імʼя та прізвище. Через пробіл. \nСпробуй ще раз";
            } else {
                String firstname = nameData[0];
                String lastname = nameData[1];
                user.setFirstName(firstname);
                user.setLastName(lastname);
                feedbackText = "Обери спеціальність: ";

                var specialities = new ArrayList<>(List.of(Speciality.values()));
                specialities.remove(Speciality.BOT_OWNER);

                keyboardMarkup = builder
                        .buildVariableMenu(
                                specialities
                                        .stream()
                                        .map(Enum::name)
                                        .toList(),
                                true);
                session.setConversationState(RegistrationStatus.ROLE);
            }

        } else if (registrationStatus.equals(RegistrationStatus.ROLE)) {
            if (
                    Arrays.stream(Speciality.values())
                     .map(Enum::name)
                     .toList()
                     .contains(inputText)
            ) {
                user.setSpeciality(Speciality.valueOf(inputText));
                user.setIsRoot(
                        inputText.equals(Speciality.MANAGEMENT.name())
                                || inputText.equals(Speciality.BOT_OWNER.name())
                );
                feedbackText = "Введи значення погодинної ставки. Я очікую щось типу ʼ85ʼ, або ʼ85.75ʼ";
                session.setConversationState(RegistrationStatus.RATE);

            } else {
                feedbackText = "нема такої спеціальності, спробуй ще раз";
            }


        } else if (registrationStatus.equals(RegistrationStatus.RATE)) {
            double enteredRate = Double.parseDouble(inputText);

            user.setHourlyRate(enteredRate);
            feedbackText = "Перевірь всi дані. Застосуй зміни або почни спочатку кнопками нижче \n"
                    + user.toStringUI();
            keyboardMarkup = builder.buildVariableMenu(
                    List.of(APPLY_BUTTON, DENY_BUTTON),
                    true
            );
            session.setConversationState(RegistrationStatus.VALIDATING);

        } else if (registrationStatus.equals(RegistrationStatus.VALIDATING)) {
            switch (inputText) {
                case APPLY_BUTTON -> {
                    if (userService.create(session.getUser()) != null) {
                        feedbackText = "Юзер був успішно доданий!";
                        user.setRegistered(true);

                        userService.update(session.getUser());
                        sessionService.dropToDefaultDialogStateAndSafe(session);

                        keyboardMarkup = builder.buildVariableMenu(commands.commandsList, true);

                    } else feedbackText = "шось пішло не так. Шоб спробувати ще раз тицьни ";
                }
                case DENY_BUTTON -> {
                    session.setConversationState(RegistrationStatus.NEW);
                    userService.delete(user.getId());
                    user = null;
                    handle(dispatchedRequest);
                }
                default -> {
                    feedbackText = "очікую що ти тицьнеш на якусь з кнопок. Давай ще раз";
                    session.setConversationState(RegistrationStatus.RATE);
                    handle(dispatchedRequest);
                }
            }

        }
        userService.update(user);
        sessionService.updateSession(session);
        telegramService.sendMessage(chatId, feedbackText, keyboardMarkup);
    }

    @Override
    public boolean isGlobal() {
        return false;
    }

    public enum RegistrationStatus {
        NEW,
        NAME,
        ROLE,
        RATE,
        VALIDATING,
        REGISTERED_SUCCESSFULLY
    }
}
