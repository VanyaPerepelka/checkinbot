package org.dubler.handlers;

import org.dubler.services.implementation.ShiftService;
import org.dubler.services.implementation.UserService;
import org.dubler.services.implementation.UserSessionService;
import org.dubler.services.telegram.TelegramService;
import org.dubler.services.telegram.utils.KeyboardBuilder;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
public abstract class AbstractScheduledHandler {

    protected TelegramService telegramService;

    protected UserService userService;

    protected KeyboardBuilder builder;

    protected UserSessionService sessionService;

    protected ShiftService shiftService;

    protected ApplicationContext context;

}
