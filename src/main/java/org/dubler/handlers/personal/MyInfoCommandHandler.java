package org.dubler.handlers.personal;

import org.dubler.constants.Commands;
import org.dubler.handlers.AbstractUserRequestHandler;
import org.dubler.services.telegram.utils.KeyboardBuilder;
import org.dubler.models.entities.User;
import org.dubler.models.request.UserRequest;
import org.dubler.services.implementation.UserService;
import org.dubler.services.telegram.TelegramService;
import org.springframework.stereotype.Component;

@Component
public class MyInfoCommandHandler extends AbstractUserRequestHandler {

    public MyInfoCommandHandler(UserService userService,
                                TelegramService telegramService,
                                Commands commands,
                                KeyboardBuilder builder
    ) {
        this.userService = userService;
        this.telegramService = telegramService;
        this.commands = commands;
        this.builder = builder;
    }

    @Override
    public boolean isApplicable(UserRequest request) {
        return isCommand(request.getUpdate(), Commands.MY_DATA);
    }

    @Override
    public void handle(UserRequest dispatchedRequest) {
        String feedbackText;
        User user = userService.getUserByChatId(dispatchedRequest.getChatId());
        feedbackText = user.toStringUI();
        var keyboard = builder.buildVariableMenu(commands.commandsList, true);
        telegramService.sendMessage(dispatchedRequest.getChatId(), feedbackText, keyboard);
    }

    @Override
    public boolean isGlobal() {
        return false;
    }
}
