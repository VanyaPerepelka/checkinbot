package org.dubler.handlers.personal;

import org.dubler.constants.Commands;
import org.dubler.handlers.AbstractUserRequestHandler;
import org.dubler.models.entities.Shift;
import org.dubler.models.entities.User;
import org.dubler.models.request.UserRequest;
import org.dubler.models.session.UserSession;
import org.dubler.services.implementation.ShiftService;
import org.dubler.services.implementation.UserService;
import org.dubler.services.implementation.UserSessionService;
import org.dubler.services.telegram.TelegramService;
import org.dubler.services.telegram.utils.KeyboardBuilder;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UserShiftsInfoHandler extends AbstractUserRequestHandler {

    public UserShiftsInfoHandler(ShiftService shiftService,
                                 UserService userService,
                                 TelegramService telegramService,
                                 KeyboardBuilder builder,
                                 Commands commands,
                                 UserSessionService sessionService){
        this.userService = userService;
        this.shiftService = shiftService;
        this.telegramService = telegramService;
        this.builder = builder;
        this.commands = commands;
        this.sessionService = sessionService;
    }
    @Override
    public boolean isApplicable(UserRequest request) {
        return request.getUserSession().getUser().isRegistered() &&
                isCommand(request.getUpdate(), Commands.STATISTICS);
    }

    @Override
    public void handle(UserRequest dispatchedRequest) {
        Long chatId = dispatchedRequest.getChatId();
        UserSession session = sessionService.getSession(chatId);
        User user = session.getUser();

        List<Shift> shiftsToReport = shiftService.getShiftsByUserId(user.getId());

        StringBuilder report = new StringBuilder().append(user.toStringUI()).append("\n");

        if (shiftsToReport.isEmpty()){
            report.append("{ще не було чекінів}");
        } else {
            shiftsToReport.forEach(shift -> report.append("\n").append(shift).append("\n"));
        }

        telegramService.sendMessage(user.getChatId(), String.valueOf(report));

    }


    @Override
    public boolean isGlobal() {
        return true;
    }
}
