package org.dubler.handlers;

import org.dubler.constants.Commands;
import org.dubler.services.telegram.utils.KeyboardBuilder;
import org.dubler.models.request.UserRequest;
import org.dubler.services.implementation.ShiftService;
import org.dubler.services.implementation.UserService;
import org.dubler.services.telegram.TelegramService;
import org.dubler.services.implementation.UserSessionService;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.Update;

@Component
public abstract class AbstractUserRequestHandler {

    protected TelegramService telegramService;
    protected UserService userService;
    protected KeyboardBuilder builder;
    protected UserSessionService sessionService;
    protected ShiftService shiftService;

    protected Commands commands;


    public abstract boolean isApplicable(UserRequest request);
    public abstract void handle(UserRequest dispatchedRequest);
    public abstract boolean isGlobal();

    protected boolean isCommand(Update update, String command) {
        return update.hasMessage() && update.getMessage().getText().equals(command);
    }
}
