package org.dubler.handlers.global;

import lombok.extern.slf4j.Slf4j;
import org.dubler.constants.Commands;
import org.dubler.handlers.AbstractUserRequestHandler;
import org.dubler.models.request.UserRequest;
import org.dubler.models.session.UserSession;
import org.dubler.services.implementation.UserService;
import org.dubler.services.telegram.TelegramService;
import org.dubler.services.telegram.utils.FileParser;
import org.dubler.services.telegram.utils.KeyboardBuilder;
import org.springframework.stereotype.Component;

import java.io.*;

@Component
@Slf4j
public class CommandsInfoCommandHandler extends AbstractUserRequestHandler {

    public CommandsInfoCommandHandler(UserService userService,
                                      TelegramService telegramService,
                                      Commands commands,
                                      KeyboardBuilder builder
    ) {
        this.userService = userService;
        this.telegramService = telegramService;
        this.commands = commands;
        this.builder = builder;
    }

    @Override
    public boolean isApplicable(UserRequest request) {
        return request.getUpdate().getMessage().getText().equals(Commands.ABOUT_BOT);
    }

    @Override
    public void handle(UserRequest dispatchedRequest){
        UserSession session = dispatchedRequest.getUserSession();
        Long chatId = session.getChatId();

        String feedbackText = FileParser.getTextFromFile(new File("commands_manual.txt"));
        telegramService.sendMessage(chatId, feedbackText);
    }

    @Override
    public boolean isGlobal() {
        return true;
    }
}
