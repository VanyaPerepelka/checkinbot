package org.dubler.handlers.global;

import org.dubler.constants.Commands;
import org.dubler.handlers.AbstractUserRequestHandler;
import org.dubler.models.entities.Shift;
import org.dubler.models.entities.User;
import org.dubler.models.request.UserRequest;
import org.dubler.models.session.UserSession;
import org.dubler.services.implementation.ShiftService;
import org.dubler.services.implementation.UserService;
import org.dubler.services.implementation.UserSessionService;
import org.dubler.services.telegram.TelegramService;
import org.dubler.services.telegram.utils.KeyboardBuilder;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;

import java.time.LocalDate;
import java.util.List;

@Component
public class NotifyAllCommandHandler extends AbstractUserRequestHandler {

    public NotifyAllCommandHandler(TelegramService telegramService,
                                   UserService userService,
                                   ShiftService shiftService,
                                   UserSessionService sessionService,
                                   Commands commands,
                                   KeyboardBuilder builder
    ){
        this.sessionService = sessionService;
        this.userService = userService;
        this.telegramService = telegramService;
        this.commands = commands;
        this.builder = builder;
        this.shiftService = shiftService;
    }
    @Override
    public boolean isApplicable(UserRequest request) {
        return (isCommand(request.getUpdate(), Commands.NOTIFY_ALL) &&
                request
                        .getUserSession()
                        .getUser()
                        .isRegistered())
                ||
                request.
                        getUserSession()
                        .getConversationState()
                        .equals(State.MESSAGE_ACCEPTED);
    }

    @Override
    public void handle(UserRequest dispatchedRequest) {
       Long chatId = dispatchedRequest.getChatId();
       UserSession session = dispatchedRequest.getUserSession();
       User user = session.getUser();
       String inputText = user.getFirstName() + " [@" + user.getUsername() + "] - "
               + dispatchedRequest
               .getUpdate()
               .getMessage()
               .getText();
       ReplyKeyboardMarkup keyboardMarkup;

       var conversationState = session.getConversationState();
       String feedbackText;

       keyboardMarkup = builder.buildVariableMenu(commands.commandsList, true);

        List<User> recipients = shiftService
                .getShiftsByDate(LocalDate.now())
                .stream()
                .map(Shift::getUser)
                .toList();

       if (!conversationState.equals(State.MESSAGE_ACCEPTED)) {

           feedbackText = "Введи месседж, який треба всім донести, " +
                   " повідомлення побачать всі зачекінені юзери.\n" +
                   " Якщо треба передати певні файли - прикріпляй до повідомлення";
           session.setConversationState(State.MESSAGE_ACCEPTED);

           telegramService.sendMessage(chatId, feedbackText);
           sessionService.updateSession(session);
       }

       if (conversationState.equals(State.MESSAGE_ACCEPTED)){
           feedbackText = "месседж був відпправлений";

           telegramService.sendMessageToAll(recipients, inputText);
           telegramService.sendMessage(chatId, feedbackText, keyboardMarkup);
           sessionService.dropToDefaultDialogStateAndSafe(session);
       }
    }
    @Override
    public boolean isGlobal() {
        return false;
    }
    private enum State{
        WAITING_FOR_MESSAGE,
        MESSAGE_ACCEPTED
    }
}
