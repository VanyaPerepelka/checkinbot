package org.dubler.handlers.global;

import lombok.extern.slf4j.Slf4j;
import org.dubler.constants.Commands;
import org.dubler.services.telegram.utils.KeyboardBuilder;
import org.dubler.handlers.AbstractUserRequestHandler;
import org.dubler.models.entities.User;
import org.dubler.models.request.UserRequest;
import org.dubler.models.session.UserSession;
import org.dubler.services.telegram.TelegramService;
import org.dubler.services.implementation.UserService;
import org.dubler.services.implementation.UserSessionService;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;

import java.util.List;

@Component
@Slf4j
public class StartCommandHandler extends AbstractUserRequestHandler {

    private static final String START_COMMAND = "/start";

    public StartCommandHandler(TelegramService service,
                               UserService userService,
                               KeyboardBuilder builder,
                               UserSessionService sessionService,
                               Commands commands){
        this.commands = commands;
        this.telegramService = service;
        this.userService = userService;
        this.sessionService = sessionService;
        this.builder = builder;
    }
    @Override
    public boolean isApplicable(UserRequest request) {
        return isCommand(request.getUpdate(), START_COMMAND);
    }

    @Override
    public void handle(UserRequest dispatchedRequest) {
        Long chatId = dispatchedRequest.getChatId();
        String feedbackText;
        User user = userService.getUserByChatId(chatId);
        ReplyKeyboardMarkup keyboardMarkup;
        UserSession session = dispatchedRequest.getUserSession();
        if (!user.isRegistered()){
            feedbackText = "Йо! Тебе нема в моїй базі даних, давай зарегаєм тебе. Обирай реестрацію в меню нижче";
            keyboardMarkup = builder.buildVariableMenu(List.of(Commands.NEW_USER), true);
        } else {
            log.info("user found [{}]", user);
            feedbackText = "Ти вже записаний в мене з наступними даними [" + user.toStringUI() + "]";
            keyboardMarkup = builder.buildVariableMenu(commands.commandsList, false);
        }
        sessionService.dropToDefaultDialogStateAndSafe(session);
        telegramService.sendMessage(chatId, feedbackText, keyboardMarkup);
    }

    @Override
    public boolean isGlobal() {
        return true;
    }
}
