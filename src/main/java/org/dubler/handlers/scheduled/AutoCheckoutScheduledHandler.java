package org.dubler.handlers.scheduled;

import lombok.extern.slf4j.Slf4j;
import org.dubler.handlers.AbstractScheduledHandler;
import org.dubler.handlers.shift.CheckOutCommandHandler;
import org.dubler.models.entities.Shift;
import org.dubler.services.implementation.ShiftService;
import org.dubler.services.telegram.TelegramService;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

@Component
@Slf4j
public class AutoCheckoutScheduledHandler extends AbstractScheduledHandler {

    private final LocalDateTime checkoutTime = LocalDateTime.of(LocalDate.now(), LocalTime.of(21, 30));
    public AutoCheckoutScheduledHandler(TelegramService telegramService,
                                        ShiftService shiftService,
                                        ApplicationContext context
                                        ){
        this.telegramService = telegramService;
        this.shiftService = shiftService;
        this.context = context;
    }

    @Scheduled(cron = "59 59 23 * * *")
    public final void checkOutAll() {

        List<Shift> toCheckout = shiftService.getShiftsByDate(LocalDate.now());
        toCheckout
                .stream()
                .filter(shift -> shift.getEndTime() == null)
                .forEach(shift -> {
                    telegramService.sendMessage(shift.getUser().getChatId(), "Ти забув чекаут, він був записаний автоматично.");

                    getCheckoutHandler().checkout(shift.getUser().getSession(), shift, checkoutTime);

                    log.info("auto-checkout for {}", shift);
                    shiftService.update(shift);
                });
    }

    private CheckOutCommandHandler getCheckoutHandler(){
        return context.getBean(CheckOutCommandHandler.class);
    }

}
