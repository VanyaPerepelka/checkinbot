package org.dubler.handlers.scheduled;

import lombok.extern.slf4j.Slf4j;
import org.dubler.enums.Speciality;
import org.dubler.handlers.AbstractScheduledHandler;
import org.dubler.models.entities.Shift;
import org.dubler.models.entities.User;
import org.dubler.services.implementation.ShiftService;
import org.dubler.services.implementation.UserService;
import org.dubler.services.implementation.UserSessionService;
import org.dubler.services.telegram.TelegramService;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

@Component
@Slf4j
public class SalaryReportScheduledHandler extends AbstractScheduledHandler {

    public SalaryReportScheduledHandler(ShiftService shiftService,
                                        TelegramService telegramService,
                                        UserSessionService sessionService,
                                        ApplicationContext context,
                                        UserService userService) {
        this.context = context;
        this.shiftService = shiftService;
        this.telegramService = telegramService;
        this.userService = userService;
        this.sessionService = sessionService;
    }

    @Scheduled(cron = "0 0 7 * * *")
    public final void sendDailyReport() {
        LocalDate dayToReport = LocalDate.now().minusDays(1);
        List<Shift> toReport = shiftService.getShiftsByDate(dayToReport)
                .stream()
                .sorted(Comparator.comparing(o -> o.getUser().getSpeciality().name()))
                .toList();

        StringBuilder report = new StringBuilder()
                .append("Чекіновий звіт за [")
                .append(LocalDate
                        .now()
                        .minusDays(1)
                )
                .append("]\n")
                .append("\n");

        toReport.forEach(shift -> report.append(shift).append("\n").append("\n"));

        List<User> recipient = userService.getUsersBySpeciality(Speciality.BOT_OWNER);

        telegramService.sendMessageToAll(recipient, String.valueOf(report));
        log.info("Daily report was formed and send to \n" + recipient + ". \n report: {}", report);
    }

    @Scheduled(cron = "1 10 7 1,16 * *")
    public final void updateBalancesAndFormSalaryReport(){
       StringBuilder report = new StringBuilder();
       AtomicReference<StringBuilder> personalReport = new AtomicReference<>(new StringBuilder());
       List<User> recipients = userService
               .getUsersBySpeciality(Speciality.BOT_OWNER);

       List<User> usersToUpdate = userService.getAll()
               .stream()
               .filter(User::isRegistered)
               .sorted(Comparator.comparing(user -> user.getSpeciality().name()))
               .toList();

        usersToUpdate.forEach(user -> {
            report
                    .append("\n")
                    .append(user)
                    .append("\n");

            personalReport.set(new StringBuilder());
            personalReport.get()
                    .append("Йоу! Тобі нараховано")
                    .append("\n")
                    .append(String.format("%,.2f", user.getTotalBalance()))
                    .append(" UAH")
                    .append("\n")
                    .append("Дякую за твою роботу! :)");

            user.setTotalBalance(0.0D);
            user.setWorkHours(0L);
            user.setPrepayment(0D);
            userService.update(user);
            sessionService.updateSession(user.getSession());
            telegramService.sendMessage(user.getChatId(), String.valueOf(personalReport));
        });

        telegramService.sendMessageToAll(recipients, String.valueOf(report));

    }
}
