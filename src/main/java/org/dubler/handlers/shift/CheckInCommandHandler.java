package org.dubler.handlers.shift;

import org.dubler.constants.Commands;
import org.dubler.handlers.AbstractUserRequestHandler;
import org.dubler.services.telegram.utils.KeyboardBuilder;
import org.dubler.models.entities.Shift;
import org.dubler.models.entities.User;
import org.dubler.models.request.UserRequest;
import org.dubler.models.session.UserSession;
import org.dubler.services.implementation.ShiftService;
import org.dubler.services.implementation.UserService;
import org.dubler.services.telegram.TelegramService;
import org.dubler.services.implementation.UserSessionService;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Objects;

@Component
public class CheckInCommandHandler extends AbstractUserRequestHandler {

    public CheckInCommandHandler(ShiftService shiftService,
                                 UserService userService,
                                 UserSessionService sessionService,
                                 TelegramService telegramService,
                                 KeyboardBuilder builder,
                                 Commands commands) {

        this.shiftService = shiftService;
        this.telegramService = telegramService;
        this.userService = userService;
        this.commands = commands;
        this.builder = builder;
        this.sessionService = sessionService;
    }

    public boolean isApplicable(UserRequest request) {
        return isCommand(request.getUpdate(), Commands.START_SHIFT);
    }

    @Override
    public void handle(UserRequest dispatchedRequest) {
        Long chatId = dispatchedRequest.getChatId();
        String feedbackText;
        UserSession session = sessionService.getSession(chatId);

        ReplyKeyboardMarkup keyboardMarkup = builder.buildVariableMenu(commands.commandsList, true);

        User user = session.getUser();
        Shift lastWrittenUsersShift = shiftService.getUserShiftByDate(LocalDateTime.now(), user.getId());

        boolean isRequestValid = (
                lastWrittenUsersShift == null ||
                        !Objects.equals(lastWrittenUsersShift.getStartTime().toLocalDate(), LocalDate.now())
                );
        if (isRequestValid){
            Shift newShift = Shift.builder()
                    .user(user)
                    .startTime(LocalDateTime.now())
                    .build();

            newShift = shiftService.create(newShift);
            if (newShift == null){
                feedbackText = "щось піщло не так. перешли джоничу це повідомлення :)";
            } else {
                feedbackText = "Твій чек ін записаний)\n" + newShift;
                session.setConversationState(CheckInState.CHECK);
            }
        } else {
            feedbackText = "Знайшов твій чек ін за сьогодні" + lastWrittenUsersShift;
        }

        telegramService.sendMessage(chatId, feedbackText, keyboardMarkup);
        sessionService.updateSession(session);
    }

    @Override
    public boolean isGlobal() {
        return false;
    }

    public enum CheckInState{
        CHECK
    }
}
