package org.dubler.handlers.shift;

import org.dubler.constants.Commands;
import org.dubler.handlers.AbstractUserRequestHandler;
import org.dubler.services.telegram.utils.KeyboardBuilder;
import org.dubler.models.entities.Shift;
import org.dubler.models.entities.User;
import org.dubler.models.request.UserRequest;
import org.dubler.models.session.UserSession;
import org.dubler.services.implementation.ShiftService;
import org.dubler.services.implementation.UserService;
import org.dubler.services.telegram.TelegramService;
import org.dubler.services.implementation.UserSessionService;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;

import java.time.LocalDateTime;

@Component
public class CheckOutCommandHandler extends AbstractUserRequestHandler {

    public CheckOutCommandHandler(ShiftService shiftService,
                                 UserService userService,
                                 UserSessionService sessionService,
                                 TelegramService telegramService,
                                 KeyboardBuilder builder,
                                 Commands commands) {

        this.shiftService = shiftService;
        this.telegramService = telegramService;
        this.userService = userService;
        this.commands = commands;
        this.builder = builder;
        this.sessionService = sessionService;
    }
    @Override
    public boolean isApplicable(UserRequest request) {
        return isCommand(request.getUpdate(), Commands.CLOSE_SHIFT);
    }

    @Override
    public void handle(UserRequest dispatchedRequest) {
        Long chatId = dispatchedRequest.getChatId();
        String feedbackText;
        UserSession session = sessionService.getSession(chatId);
        User user = session.getUser();
        ReplyKeyboardMarkup keyboardMarkup = builder.buildVariableMenu(commands.commandsList, true);

        Shift lastCheckIn = shiftService.getUserShiftByDate(LocalDateTime.now(), user.getId());

        boolean isRequestValid = lastCheckIn != null;
        boolean isCheckOutWas = lastCheckIn != null && lastCheckIn.getEndTime() != null;

        if (isRequestValid){

            if (isCheckOutWas){
                feedbackText = "Твоя остання операція щодо чекауту була сьогодні або чекаут стягнувся автоматично, ось що знайшов:" + lastCheckIn + "]";

            } else {
                Shift shiftToUpdate = checkout(session, lastCheckIn, LocalDateTime.now());
                feedbackText = "Чудова робота! Твоя зміна сьогодні: \n" + shiftToUpdate + "\n Не забудь гарно відпочити!";
            }

        } else {
           feedbackText = "Не бачу твого чекіну. Сьогодні доведеться вписати ручками, наступного разу не забувай :)";
        }

        telegramService.sendMessage(chatId, feedbackText, keyboardMarkup);
        sessionService.updateSession(session);
    }

    public Shift checkout(UserSession session, Shift shift, LocalDateTime checkoutTime){
        User user = session.getUser();


        var totalMinuets = ShiftService.calculateTotalMinuetsPerShift(shift.getStartTime(), checkoutTime);
        var newUsersBalance = ShiftService.calculateEmployeeIncome(totalMinuets, user);

        shift.setEndTime(checkoutTime);
        shift.setTotalMinuets(totalMinuets);

        user.setTotalBalance(newUsersBalance);
        user.setWorkHours(Math.addExact(user.getWorkHours(), totalMinuets/60)) ;

        Shift shiftToUpdate = shiftService.update(shift);

        userService.update(user);
        shiftService.update(shiftToUpdate);
        sessionService.dropToDefaultDialogStateAndSafe(session);

        return shiftToUpdate;
    }

    @Override
    public boolean isGlobal() {
        return false;
    }

}
