package org.dubler.models.request;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;
import org.dubler.models.session.UserSession;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.io.Serializable;

@Data
@Builder
@JsonDeserialize
public class UserRequest implements Serializable {

    @ToString.Exclude
    private Update update;

    private Long chatId;

    private UserSession userSession;
}
