package org.dubler.models.entities;

import lombok.*;
import org.dubler.enums.Speciality;
import org.dubler.models.session.UserSession;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Table(name = "users_table")
@Getter
@Setter
@Entity
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    @ToString.Exclude
    private Long id;

    @Column(name = "chat_id", nullable = false)
    private Long chatId;

    @Column(name = "username", unique = true)
    private String username;

    @Column(name = "firstname")
    private String firstName;

    @Column(name = "lastname")
    private String lastName;

    @Column(name = "total_balance")
    private Double totalBalance;

    @Column(name = "prepayment")
    private Double prepayment;

    @Column(name = "rate")
    private Double hourlyRate;

    @Column(name = "work_hours")
    private Long workHours;

    @Column(name = "is_root")
    private Boolean isRoot;

    @Column(name = "speciality")
    @Enumerated(EnumType.STRING)
    private Speciality speciality;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user", fetch = FetchType.EAGER)
    @ToString.Exclude
    private Set<Shift> shifts;

    @OneToOne(mappedBy = "user", cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn
    private UserSession session;

    private boolean isRegistered;

    public final String toStringUI(){
        return "\n[прізвище] " + this.lastName +
                "\n[імʼя] " + this.firstName +
                "\n[часова ставка] " + this.hourlyRate +
                "\n[дублерна спеціальність] " + speciality.name() +
                "\n[нараховано кешу] " + totalBalance;

    }
}
