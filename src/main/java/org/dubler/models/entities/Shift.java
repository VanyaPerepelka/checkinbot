package org.dubler.models.entities;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Table(name = "shifts_table")
@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Shift  implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "shift_id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "start_time")
    private LocalDateTime startTime;

    @Column(name = "end_time")
    private LocalDateTime endTime;

    @Column(name = "total_minuets")
    private Long totalMinuets;

    @Override
    public String toString(){
        StringBuilder builder = new StringBuilder();
        String end;
        String totalHours;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");

        if (endTime == null){
            end = "{не було чекауту}";
            totalHours = "{не було чекауту}";
        } else {
            totalHours = String.valueOf(totalMinuets / 60L);
            end = formatter.format(endTime);
        }

        return builder
                .append("\n[")
                .append(user.getFirstName())
                .append(" ")
                .append(user.getLastName())
                .append("]\n")
                .append("дата: ")
                .append(startTime.toLocalDate())
                .append("\nчас чекіну: ")
                .append(formatter.format(startTime))
                .append("\n")
                .append("час чекауту: ")
                .append(end)
                .append("\n")
                .append("робочих годин: ")
                .append(totalHours)
                .toString();
    }

}
