package org.dubler.models.session;

import lombok.*;
import org.dubler.models.entities.User;

import javax.persistence.*;
import java.io.Serializable;

@Table(name = "session_table")
@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserSession implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "chat_id")
    private Long chatId;

    @OneToOne
    @MapsId
    @JoinColumn(name = "id", referencedColumnName = "user_id")
    private User user;

    @Column(name = "conversation_state")
    private Enum<?> conversationState;

    public enum DefaultEnum{
        DEFAULT
    }
}
