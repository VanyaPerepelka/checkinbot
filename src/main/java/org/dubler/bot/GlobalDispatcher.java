package org.dubler.bot;

import org.dubler.constants.Commands;
import org.dubler.handlers.AbstractUserRequestHandler;
import org.dubler.services.telegram.utils.KeyboardBuilder;
import org.dubler.models.entities.User;
import org.dubler.models.request.UserRequest;
import org.dubler.models.session.UserSession;
import org.dubler.services.telegram.TelegramService;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;

import java.util.Comparator;
import java.util.List;

@Component
public class GlobalDispatcher {

    private final List<AbstractUserRequestHandler> handlers;

    private final TelegramService telegramService;

    private final Commands commands;

    private final KeyboardBuilder builder;

    public GlobalDispatcher(List<AbstractUserRequestHandler> handlers,
                            TelegramService telegramService,
                            Commands commands,
                            KeyboardBuilder builder){
        this.handlers = handlers
                .stream()
                .sorted(
                        Comparator
                        .comparing(AbstractUserRequestHandler::isGlobal)
                        .reversed())
                .toList();
        this.telegramService = telegramService;
        this.commands = commands;
        this.builder = builder;
    }
    public boolean dispatch(UserRequest request){
        if (!isInvalidRequests(request)){
            for (AbstractUserRequestHandler handler : handlers) {
                if (handler.isApplicable(request)){
                    handler.handle(request);
                    return true;
                }
            }
        }
        return false;
    }

    private boolean isInvalidRequests(UserRequest request){
        ReplyKeyboardMarkup keyboardMarkup;
        UserSession session = request.getUserSession();
        User user = session.getUser();
        String feedbackText ="";

        if (session.getConversationState().equals(UserSession.DefaultEnum.DEFAULT) &&
                commands.notApplicableCommand(request.getUpdate().getMessage().getText())) {

            if (!user.isRegistered()){
                keyboardMarkup = builder.buildVariableMenu(List.of(Commands.NEW_USER), true);
            } else {
                keyboardMarkup = builder.buildVariableMenu(commands.commandsList, false);
            }
            telegramService.sendMessage(request.getChatId(), feedbackText, keyboardMarkup);
            return true;
        } else return false;
    }
}

