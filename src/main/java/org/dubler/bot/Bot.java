package org.dubler.bot;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.dubler.models.request.UserRequest;
import org.dubler.models.session.UserSession;
import org.dubler.services.telegram.TelegramService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.dubler.services.implementation.UserSessionService;

import javax.transaction.Transactional;

@Slf4j
@Component
@Getter
public class Bot extends TelegramLongPollingBot {

    @Value("${bot.token}")
    private String botToken;

    @Value("${bot.username}")
    private String botUsername;

    private final GlobalDispatcher dispatcher;

    private final UserSessionService userSessionService;

    private final TelegramService telegramService;

    public Bot(GlobalDispatcher dispatcher, UserSessionService service, TelegramService telegramService) {
        this.dispatcher = dispatcher;
        this.userSessionService = service;
        this.telegramService = telegramService;
    }

    /**
     * This is an entry point for any messages, or updates received from user<br>
     * Docs for "Update object: <a href="https://core.telegram.org/bots/api#update">link</a>
     */
    @Override
    public void onUpdateReceived(Update update) {
        if (update.hasMessage() && update.getMessage().hasText()) {
            String  textFromUser =update.getMessage().getText();
            Long userId = update.getMessage().getFrom().getId();
            String userFirsName = update.getMessage().getFrom().getFirstName();
            Long chatId = update.getMessage().getChatId();

            log.info("[chatId {} ]: [{}, {}] : {}",chatId, userId, userFirsName, textFromUser);

            UserSession session = userSessionService.getSession(chatId);

            UserRequest request = UserRequest
                    .builder()
                    .update(update)
                    .chatId(chatId)
                    .userSession(session)
                    .build();

            boolean isDispatched = dispatcher.dispatch(request);

            if (!isDispatched) {
                log.error("There is no handler for [{}]", request);
                telegramService.sendMessage(chatId, "Не можу захендлити твій реквест");
            }
        } else log.error("Error update [{}]", update);
    }

    @Override
    public String getBotUsername() {
        return botUsername;
    }

    @Override
    public String getBotToken() {
        return botToken;
    }
}
