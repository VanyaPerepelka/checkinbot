package org.dubler.services.telegram;

import lombok.extern.slf4j.Slf4j;
import org.dubler.constants.Commands;
import org.dubler.models.entities.User;
import org.dubler.sender.Sender;
import org.dubler.services.telegram.utils.KeyboardBuilder;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.methods.send.SendDocument;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.InputFile;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboard;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.time.LocalDate;
import java.util.List;
import java.io.File;

@Component
@Slf4j
public class TelegramService {

    private final Sender sender;

    private final KeyboardBuilder builder;

    private final Commands commands;

    public TelegramService(Sender sender, KeyboardBuilder builder, Commands commands){
        this.sender = sender;
        this.builder = builder;
        this.commands = commands;
    }
    public void sendMessage(Long chatId, String text) {
        sendMessage(chatId, text, builder.buildVariableMenu(commands.commandsList, true));
    }

    public void sendMessage(Long chatId, String text, ReplyKeyboard replyKeyboard) {
        SendMessage sendMessage = SendMessage
                .builder()
                .text(text)
                .chatId(chatId.toString())
                //Other possible parse modes: MARKDOWNV2, MARKDOWN, which allows to make text bold, and all other things
                .parseMode(ParseMode.HTML)
                .replyMarkup(replyKeyboard)
                .build();
        execute(sendMessage);
    }

    public void sendDocument(Long chatId, File document, ReplyKeyboard replyKeyboard){
        SendDocument sendDocumentReq = SendDocument
                .builder()
                .document(new InputFile(LocalDate.now() + " запит на чек ін").setMedia(document))
                .chatId(chatId.toString())
                .parseMode(ParseMode.HTML)
                .replyMarkup(replyKeyboard)
                .build();
        try {
            sender.execute(sendDocumentReq);
        } catch (TelegramApiException e) {
            log.error("Can't execute file ", e);
        }
    }

    public void sendMessageToAll(List<User> users, String text){
        users.forEach(user -> sendMessage(user.getChatId(), text));
    }

    private void execute(BotApiMethod method){
        try{
            sender.executeAsync(method);
        } catch (TelegramApiException e) {
            log.error("Can't execute method [{}] : {}", method, e);
        }
    }
}
