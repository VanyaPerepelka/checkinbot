package org.dubler.services.telegram.utils;

import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;

import java.util.ArrayList;
import java.util.List;

@Component
public class KeyboardBuilder {

    public ReplyKeyboardMarkup buildVariableMenu(List<String> commands, boolean isOneTimeKeyboard) {
        List<KeyboardRow> rows = new ArrayList<>();
        commands
                .stream()
                .map(KeyboardButton::new)
                .forEach(button -> rows.add(new KeyboardRow(List.of(button))));

        return ReplyKeyboardMarkup.builder()
                .keyboard(rows)
                .selective(true)
                .resizeKeyboard(true)
                .oneTimeKeyboard(isOneTimeKeyboard)
                .build();
    }
}
