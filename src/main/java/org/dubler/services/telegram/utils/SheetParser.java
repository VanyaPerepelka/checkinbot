package org.dubler.services.telegram.utils;

import lombok.extern.slf4j.Slf4j;
import org.dubler.models.entities.Shift;
import org.dubler.models.entities.User;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

@Slf4j
public class SheetParser {

    private SheetParser(){}
    public static File parseSheetData(List<User> users) {
        HashMap<User, Set<Shift>> allShifts = new HashMap<>();

        users.sort((o1, o2) -> o1.getFirstName().compareTo(o2.getLastName()));
        users.forEach(employee -> allShifts.put(employee, employee.getShifts()));

        File file = new File(LocalDate.now() + " checkins request.txt");
        try (FileWriter writer = new FileWriter(file)) {
            final StringBuilder[] builder = new StringBuilder[1];
            allShifts.forEach((user, usersShiftSet) ->
                    {
                        var string = builder[0] = new StringBuilder(); //effectively final obj
                        string.append("\n [").append(user.getFirstName()).append("]");

                    });
            writer.write(String.valueOf(builder[0]));
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        return file;
    }
}
