package org.dubler.services.telegram.utils;

import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

@Slf4j
public class FileParser {

    private FileParser(){}

    public static String getTextFromFile(File file) {

        StringBuilder result = new StringBuilder();
        try(BufferedReader reader = new BufferedReader(new FileReader(file));){
            reader.lines().forEach(line -> result.append(line).append("\n"));
        } catch (IOException e){
            log.error("Error handling file [{}]", file);
        }

        return String.valueOf(result);
    }
}
