package org.dubler.services.implementation;

import org.dubler.models.entities.User;
import org.dubler.models.session.UserSession;
import org.dubler.repo.UserSessionRepository;
import org.dubler.services.EntityService;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.*;

@Component
@Transactional
public class UserSessionService implements EntityService<UserSession> {

    private final UserSessionRepository sessionRepository;

    public UserSessionService(UserSessionRepository sessionRepository) {
        this.sessionRepository = sessionRepository;
    }


    @Transactional
    public UserSession getSession(Long chatId){
        var session = sessionRepository.findByChatId(chatId);
        if (session == null){
            return updateSession(UserSession
                    .builder()
                    .chatId(chatId)
                    .user(User.builder()
                            .chatId(chatId)
                            .build()
                    )
                    .conversationState(UserSession.DefaultEnum.DEFAULT)
                    .build());
        } else return session;
    }

    public UserSession updateSession(@NotNull UserSession session) {
        return sessionRepository.save(session);
    }

    public UserSession dropToDefaultDialogStateAndSafe(@NotNull UserSession session){
        session.setConversationState(UserSession.DefaultEnum.DEFAULT);
        return updateSession(session);
    }

    @Override
    public UserSession readById(Long id) {
        return sessionRepository.findSessionById(id);
    }

    @Override
    public UserSession create(UserSession entity) {
        return sessionRepository.save(entity);
    }

    @Override
    public boolean delete(Long id) {
        UserSession entity = readById(id);
        sessionRepository.delete(entity);
        return true;
    }

    @Override
    public List<UserSession> getAll() {
        var all = sessionRepository.findAll();
        return all.isEmpty() ? new ArrayList<>() : all;
    }
}
