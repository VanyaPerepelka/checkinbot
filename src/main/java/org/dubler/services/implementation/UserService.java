package org.dubler.services.implementation;

import org.dubler.enums.Speciality;
import org.dubler.models.entities.User;
import org.dubler.services.EntityService;
import org.springframework.stereotype.Service;
import org.dubler.repo.UsersRepository;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class UserService implements EntityService<User> {
    private final UsersRepository usersRepository;

    public UserService(UsersRepository usersRepository){
        this.usersRepository = usersRepository;
    }

    @Override
    public User readById(Long id) {
        Optional<User> user= usersRepository.findById(id);
        return user.orElse(null);
    }

    @Override
    public User create(User entity) {
        return usersRepository.save(entity);
    }

    @Override
    public boolean delete(Long id) {
        Optional<User> user = usersRepository.findById(id);
        if (user.isPresent()){
            usersRepository.deleteById(id);
            return true;
        } else return false;
    }

    public User update(User user){
        return usersRepository.save(user);
    }

    public List<User> getUsersBySpeciality(Speciality speciality){
        return usersRepository.getUserBySpeciality(speciality.name());
    }

    public User getUserByName(String firstname, String secondName){
        return usersRepository.getUserByPersonalData(firstname, secondName);
    }

    @Override
    public List<User> getAll() {
        List<User> users = usersRepository.findAll();
        return users.isEmpty() ? new ArrayList<>() : users;
    }

    public User getUserByChatId(Long chatId){
        return usersRepository.getUserByChatId(chatId);
    }
}
