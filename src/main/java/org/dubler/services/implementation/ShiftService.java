package org.dubler.services.implementation;

import org.dubler.models.entities.Shift;
import org.dubler.models.entities.User;
import org.dubler.repo.ShiftRepository;
import org.dubler.services.EntityService;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ShiftService implements EntityService<Shift> {

    private final ShiftRepository shiftRepository;

    public ShiftService(ShiftRepository shiftRepository){
        this.shiftRepository = shiftRepository;
    }

    public static long calculateTotalMinuetsPerShift(LocalDateTime start, LocalDateTime end) {
        return ChronoUnit.MINUTES.between(start, end);
    }

    public static double calculateEmployeeIncome(Long minuets, User user) {
        return (minuets / 60.0) * user.getHourlyRate();
    }

    public Shift update(Shift shift){
        return shiftRepository.save(shift);
    }

    public List<Shift> getShiftsByUserId(Long userId){
        return shiftRepository.getShiftsByUserId(userId);
    }

    public List<Shift> getShiftsByDate(LocalDate startTime){
        List<Shift> result = shiftRepository.getShiftsByDate(startTime);
        return result.isEmpty() ? new ArrayList<>() : result;
    }

    public Shift getUserShiftByDate(LocalDateTime startTime, Long userId){
        return shiftRepository.getUserShiftByDate(startTime, userId);
    }

    @Override
    public Shift readById(Long id) {
        return shiftRepository.getById(id);
    }

    @Override
    public Shift create(Shift entity) {
        return shiftRepository.save(entity);
    }

    @Override
    public boolean delete(Long id) {
        Optional<Shift> shiftToDelete = shiftRepository.findById(id);
        if (shiftToDelete.isPresent()){
            shiftRepository.delete(shiftToDelete.get());
            return true;
        } else return false;
    }

    @Override
    public List<Shift> getAll() {
        List<Shift> all = shiftRepository.findAll();
        return all.isEmpty() ? new ArrayList<>() : all;
    }
}
