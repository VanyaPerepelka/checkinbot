package org.dubler.services;

import java.util.List;

public interface EntityService<T> {
    T readById(Long id);
    T create(T entity);
    boolean delete(Long id);
    List<T> getAll();
}
