# Restaurant Employee Working Hours Tracker Telegram Bot

## Overview

This Telegram bot application is designed to help restaurant managers and supervisors keep track of the working hours of their employees. The bot allows restaurant staff to clock in and clock out, and it provides an easy-to-use interface for managers to view and manage employee working hours. The bot is built using Java with Spring Boot and Maven, and it utilizes PostgreSQL as the database to store employee data and working hours. The application is deployed on the Heroku platform for easy accessibility and scalability.

## Features

- Employees can clock in and clock out by sending specific commands to the bot.
- Managers can view the working hours of individual employees or the entire staff.
- Ability register diferent restoraunt employee specialities.
- Work hours reports are formed and sent to the regular telegram every morning.
- Salary calculation, forming and reporting occur every two weeks and are also sent to the selected telegram.
- The bot provides statistics on total working hours per employee and the overall workforce.
- Ability to notify all clockined employees with special message.


## Getting Started

To use the Telegram bot and access the employee working hours data, follow the steps below:

1. **Prerequisites:**
   - Java Development Kit (JDK) 17 or higher installed on your system.
   - Apache Maven installed on your system.
   - PostgreSQL database credentials (username, password, database URL).

2. **Telegram Bot Setup:**
   - Create a new bot on Telegram using the BotFather. Obtain the API token for your bot.

3. **Database Setup:**
   - Create a new PostgreSQL database employee data, working hours and other utility staff.

4. **Configuration:**
   - Clone this GitHub repository and navigate to the project directory.
   - Open the `application.properties` file located in the `src/main/resources` folder.
   - Replace the placeholders with your Telegram bot API token and PostgreSQL database credentials.

    
5. **Deploy on Heroku:**
   - Create a Heroku account (if you don't have one) and install the Heroku CLI.
   - Navigate to the project directory and log in to Heroku:
     ```
     heroku login
     ```
   - Create a new Heroku app:
     ```
     heroku create <app-name>
     ```
   - Set the environment variables for your bot token, database credentials and time zone:
     ```
     heroku config:set BOT_TOKEN=<your-telegram-bot-token>
     heroku config:set DATABASE_URL=<your-database-url>
     heroku config:set TZ=<your timezone> 

     ```
   - Deploy the application to Heroku:
     ```
     git push heroku master
     ```
   - Scale the app to at least one web dyno to make it active:
     ```
     heroku ps:scale web=1
     ```

6. **Telegram Bot Usage:**
   - Search for your bot on Telegram and start a conversation.
   - Pass the registration
   - Register your utility telegram to reive reports(manualy write BOT_OWNER speciality when choosing one)

## Contributing

Contributions to the project are welcome! If you find any issues or have suggestions for improvements, please feel free to create a pull request or submit an issue on GitHub.

## Acknowledgments

Special thanks to the open-source community, the developers of Spring Boot, Maven, PostgreSQL, and Heroku for providing amazing tools to build and deploy applications.
